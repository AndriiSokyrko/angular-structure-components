import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styles: [
    'input { width:80%;font-weight: normal;margin-bottom: 15px; }'
  ]
})
export class InputComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
