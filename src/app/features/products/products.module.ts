import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product/product.component';
import {InputModule} from "../../shared/input/input.module";
import {ButtonModule} from "../../shared/button/button.module";
import { ProductsComponent } from './products/products.component';



@NgModule({
  declarations: [
    ProductComponent,
    ProductsComponent
  ],
  exports: [
    ProductComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    InputModule,
    ButtonModule,
    InputModule
  ]
})
export class ProductsModule { }
