import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {CoreModule} from "./core/core.module";
import {FeaturesModule} from "./features/features.module";
import {SharedModule} from "./shared/shared.module";
import {HeaderModule} from "./core/header/header.module";
import {FooterModule} from "./core/footer/footer.module";
import {ProductsModule} from "./features/products/products.module";
import {InputModule} from "./shared/input/input.module";
import {ButtonModule} from "./shared/button/button.module";
import {CartModule} from "./features/cart/cart.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FeaturesModule,
    SharedModule,
    HeaderModule,
    FooterModule,
    ProductsModule,
    InputModule,
    ButtonModule,
    CartModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
