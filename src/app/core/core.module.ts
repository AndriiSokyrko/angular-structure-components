import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthenticationModule} from "./authentication/authentication.module";
import {FooterModule} from "./footer/footer.module";
import {HeaderModule} from "./header/header.module";

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AuthenticationModule,
    FooterModule,
    HeaderModule
  ]
})
export class CoreModule { }
